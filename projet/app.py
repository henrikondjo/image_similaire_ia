import streamlit as st
import cv2, os, time
import numpy as np
from PIL import Image
from descriptors import bitdesc
import pandas as pd
from scipy.spatial import distance
from distances import distance_selection
from upload import upload_file

def main():
    st.title("Générateur d'images similaires")
    print("App lunched!")
    input_value = st.sidebar.number_input("Enter une valeur", min_value=1, max_value=500, value=10, step=1)
    st.sidebar.write(f"Vous avez entrez {input_value}")
    options = ["Euclidean", "Canberra", "Manhattan", "Chebyshev", "Minkowsky"]
    distance_option = st.sidebar.selectbox("Selectionnez une distance", options)
    st.sidebar.write(f"Vous avez choisit {distance_option}")
    signatures = np.load('cbir_signatures_v1.npy')
    distanceList = list()
    is_image_uploaded = upload_file()
    if is_image_uploaded:
        st.write('''
                 # Search Results
                 ''')
        query_image = 'uploaded_images/query_image.png'
        img = cv2.imread(query_image, 0)
        bit_feat = bitdesc(img)
        for sign in signatures:
            sign = np.array(sign)[0:-2].astype('float')
            sign = sign.tolist()
            distance = distance_selection(distance_option, bit_feat, sign)
            distanceList.append(distance)
        print("Ditance computed successfully")
        minDistances =list()
        for i in range(input_value):
            array = np.array(distanceList)
            index_min = np.argmin(array)
            minDistances.append(index_min)
            max = array.max()
            distanceList[index_min] = max
        print(minDistances)
        image_paths = [signatures[small][-1] for small in minDistances]
        classes = [signatures[small][-2] for small in minDistances]
        classes = np.array(classes)
        unique_values, counts = np.unique(classes, return_counts=True)
        list_classes = list()
       
        
        # Display
        
        for image_path in image_paths:
            similar_image = Image.open(image_path)
            st.image(similar_image, caption="image similare trouvé")

        print("Unique value with their counts")
        for value, count in zip(unique_values, counts):
            print(f"{value}:{count}")
            list_classes.append(value)
        df = pd.DataFrame({"Value": unique_values, "frequency":counts})
        st.bar_chart(df.set_index("Value"))    
                        
        
    else:
        st.write("Bienvenue, Veuillez choisir une image")
                                   
if __name__ == "__main__":
    main()
    